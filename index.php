<?php
include_once "controller/Controller.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Trip Builder</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>

    </style>
</head>
<body>
<div class="row" style="padding: 30px;">
    <div class="col-md-12" >
        <h1 class="" style="font-weight: bold">Trip Builder</h1>
    </div>
    <div class="col-md-12">
        <div class="col-md-4" style="padding: 20px 0;padding-right:10px; border-right: 1px solid;">

            <form style="padding: 10px 0;" >
                <h2 class="text-center" style="text-decoration: underline;padding-bottom: 20px;">Create</h2>
                <div class="form-group">
                    <label for="tripName" class="col-md-4">Trip Name: </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="tripName" name="tripName">
                    </div>
                </div>
                <h4 style="margin-top: 20px;padding-top: 40px;">Add new flight</h4>
                <div class="form-group">
                    <select class="form-control" id="from" name="from">
                        <option value="">From</option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" id="to" name="to">
                        <option value="">To</option>
                    </select>
                </div>
                <div class="text-right">
                    <button type="button" class="btn btn-default btn-primary" id="addAFlight">Add</button>
                </div>
                <div>
                    <p>Trip Info:</p>
                    <ol id="tripinfo">


                    </ol>
                </div>
                <div class="text-center">
                    <button type="button" class="btn btn-default btn-lg btn-success" id="addATrip">Save</button>
                </div>
                <!--                <div class="text-center">-->
                <!--                    <button type="button" class="btn btn-default btn-lg btn-success" id="buttonclickme">TripAirports</button>-->
                <!--                </div>-->
            </form>
        </div>
        <div class="col-md-8">
            <h2 class="text-center" style="text-decoration: underline;padding-bottom: 20px;">Edit</h2>
            <div class="form-group">
                <select class="form-control" id="trip_list" name="trip_list">
                    <option value="">-- Select a trip -- </option>
                </select>
            </div>
            <div class="form-group">
                <label for="tripName" class="col-md-2">Trip Name: </label>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="tripInfoName" name="tripInfoName">
                </div>
                <div class="col-md-2">
                    <input type="button" class="form-control btn btn-info" id="renameTrip" name="renameTrip" value="Rename">
                </div>
                <div class="col-md-2"></div>
            </div>

            <h4 id="total_flights" class="col-md-12 text-center" style="font-weight: bold;text-decoration: underline"></h4>
            <table class="col-md-12" style="margin-top: 20px;" id="flight_list">

            </table>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
    $.ajax({
        url: "controller/Controller.php?getairports",
        method:"GET",
        contentType: "application/json"
    }).done(function(data) {
        var airports = JSON.parse(data);
        // console.log(airports);
        var selectOptions = "";
        for(var i=0;i<airports.length;i++){
            selectOptions += '<option value="'+airports[i].id+'">'+airports[i].name+'</option>';
        }
        $("#from, #to").append(selectOptions);
    });

    var tripArray = [];
    var tripTextArray = [];
    var flightCount = 0;
    $( "#addAFlight" ).click(function() {
        flightCount ++;
        var tripObject = {};
        var tripTextObject = {};
        tripObject.from = $("#from").val();
        tripObject.to = $("#to").val();

        tripTextObject.from = $("#from option:selected").text();
        tripTextObject.to = $("#to option:selected").text();

        tripArray.push(tripObject);
        tripTextArray.push(tripTextObject);
        var tripinfoli="";
        for(var j = 0; j < tripTextArray.length; j++){
            tripinfoli += "<li>"+tripTextArray[j].from+" -> "+tripTextArray[j].to+"</li>";
        }
        $("#tripinfo").html("").append(tripinfoli);
    });

    $( "#addATrip" ).click(function() {
        var tripName = $("#tripName").val();
        console.log(tripName);
        $.ajax({
            url: "controller/Controller.php?addatrip",
            method:"POST",
            data:{"trip":tripArray,"name":tripName},
            //contentType: "application/json"
        }).done(function(data) {
            console.log(data);
            location.reload();
        });

    });

    function getAllTrips(){
        $.ajax({
            url: "controller/Controller.php?gettripairports",
            method:"GET",
            contentType: "application/json"
        }).done(function(data) {
            var trips = JSON.parse(data);
            var selectOptions = "";
            for(var i=0;i<trips.length;i++){
                selectOptions += '<option value="'+trips[i].id+'">'+trips[i].name+'</option>';
            }
            $("#trip_list").append(selectOptions);

        });
    }
    getAllTrips();
    var global_trip_id = "";
    $("#trip_list").on('change', function() {
        var trip_id = this.value;
        $.ajax({
            url: "controller/Controller.php?gettripinformation",
            method:"GET",
            data : {"id":trip_id},
            contentType: "application/json"
        }).done(function(data) {
            var tripInfo = JSON.parse(data);
            console.log(tripInfo);
            var no_of_flights =tripInfo.length;
            var trip_name= $("#trip_list option:selected").text();
            global_trip_id = $("#trip_list").val();
            $("#tripInfoName").val(trip_name);
            if(no_of_flights > 0){
                var flightDetails='<tr><th class="col-md-4">From</th><th class="col-md-4">To</th><th class="col-md-2"></th></tr>';
                $("#total_flights").html("You have "+no_of_flights+" flights in your trip");
                for(var i=0;i<tripInfo.length;i++){
                    flightDetails += "<tr>";
                    flightDetails += '<td class="col-md-4">'+tripInfo[i].from.name+'</td>';
                    flightDetails += '<td class="col-md-4">'+tripInfo[i].to.name+'</td>';
                    flightDetails += '<td class="col-md-2"><input type="button" class="form-control btn btn-info" id="delete_'+tripInfo[i].id+'" onclick="deleteTrip('+tripInfo[i].id+')"  value="Remove"></td>';
                    flightDetails += "</tr>";
                }
                $("#flight_list").html(flightDetails);
            }else{
                $("#total_flights").html("You have 0 flights in your trip");
                $("#flight_list").html("");
            }


        });
    });

    function deleteTrip(getid){
        var deleteid = getid;
        $.ajax({
            url: "controller/Controller.php?deleteflight",
            method:"GET",
            data : {"id":deleteid},
            contentType: "application/json"
        }).done(function(data) {
            console.log(data);
            if(data == "true"){
                alert("Flight deleted!");
            }else{
                alert("Flight not deleted!");
            }
            //location.reload();
        });
    }


    $( "#renameTrip" ).click(function() {
        var tripName= $("#tripInfoName").val();
        $.ajax({
            url: "controller/Controller.php?renameTrip",
            method:"GET",
            data:{"id":global_trip_id,"name":tripName}
            //contentType: "application/json"
        }).done(function(data) {
            console.log(data);
            alert("Trip renamed successfully!");
            location.reload();

        });

    });

</script>
</body>
</html>
