<?php

class Role extends Eloquent  {

	

	public  $table = 'roles';
    
    public function users()
    {
        return $this->hasMany('User');
    }
    
        
       
        protected $fillable = array('libelle','etat');
        public static $rules = array('libelle'=>'required');
}

