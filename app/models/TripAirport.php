<?php

class TripAirport extends Eloquent  {

    public  $table = 'trip_airport';

    public function trip()
    {
        return $this->belongsTo("Trip");
    }
    public function from()
    {
        return $this->belongsTo("Airport","from");
    }
    public function to()
    {
        return $this->belongsTo("Airport","to");
    }

    protected $fillable = array('trip_id','from','to');

    //public static $rules = array('name'=>'required');
}

