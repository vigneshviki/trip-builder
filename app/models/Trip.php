<?php

class Trip extends Eloquent  {

    public  $table = 'trips';

    protected $fillable = array('name');
    public static $rules = array('name'=>'required');

}

