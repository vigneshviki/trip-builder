<?php

class Airport extends Eloquent  {

    public  $table = 'airports';

    protected $fillable = array('name','country','city','code');
    public static $rules = array('name'=>'required');
}

