<?php

class User extends Eloquent  {

	

	public  $table = 'users';
      
    public function role()
    {
        return $this->belongsTo('Role');
    }
        
        protected $hidden = array('password');
        protected $fillable = array('firstname','lastname','email','password',
         'picture','position','region','country','site', 'description', 'contact_email', 'contact_phone','role_id');
        public static $rules = array('password'=>'required',
            'email'=>'required',
            'role_id'=>'required');
}

