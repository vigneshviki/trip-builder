<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    return View::make('hello');
});

Route::group(array('prefix'=>'airports'),function(){
    Route::get('/',array('uses'=>'AirportController@get_all'));
});
Route::group(array('prefix'=>'trips'),function(){
    Route::get('/',array('uses'=>'TripController@get_all'));
    Route::get('/getrips',array('uses'=>'TripController@get_trips'));
    Route::put('/edit/{id}',array('uses'=>'TripController@rename_trip'));
    Route::get('/{id}',array('uses'=>'TripController@get_trip_byId'));
    Route::post('/create', array('uses'=>'TripController@post_trip'));
});

Route::group(array('prefix'=>'trip_airport'),function(){
    Route::get('/',array('uses'=>'TripAirportController@get_all'));
    Route::delete('/delete/{id}', array('uses'=>'TripAirportController@delete_flight'));
});
Route::group(array('prefix'=>'users'),function(){
    Route::get('/',array('uses'=>'UserController@get_all'));
    Route::group(array('prefix'=>'/find'),function(){
        Route::get('/{id}',array('uses'=>'UserController@get_user'));
    });
    Route::post('/create', array('uses'=>'UserController@post_user'));
    Route::put('/edit/{id}', array('uses'=>'UserController@put_user'));
    Route::delete('/delete/{id}', array('uses'=>'UserController@delete_user'));
    Route::post('/login', array('uses' => 'UserController@doLogin'));
    Route::put('/activate', array('uses' => 'UserController@doRegister'));
    Route::put('/passwordreset', array('uses' => 'UserController@passwordReset'));

});


Route::group(array('prefix'=>'roles'),function(){
    Route::get('/',array('uses'=>'RoleController@get_all'));
    Route::group(array('prefix'=>'/find'),function(){
        Route::get('/{id}',array('uses'=>'RoleController@get_role'));
    });
});

