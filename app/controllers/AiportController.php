<?php

class AirportController extends BaseController {

    public $restful = true;
    public function get_all()
    {
        return Airport::get();
    }
    
}