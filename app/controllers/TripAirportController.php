<?php

class TripAirportController extends BaseController
{

    public $restful = true;

    public function delete_flight($id){

        $flight = TripAirport::find($id);
        if($flight){
            $flight->delete();
            return "true";
        }
        return "false";

    }
}