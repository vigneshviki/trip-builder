<?php
include_once '../app/controllers/Services.php';
class UserController extends BaseController {


    public $restful = true;
    public function get_all()
    {
        return User::with(['role'])->get();
    }


    public function get_user($id)
    {
        return User::with(['role'])->find($id);
    }

    public function post_user(){
        $user = Input::all();
        $v = Validator::make($user,  User::$rules);

        if($v->passes()){
            $user1 = User::where('email',$user['email'])->first();
            if(!$user1){
                $user['password'] = md5($user['password']);
                User::create($user);

                return "true";
            }else{
                return "false1";
            }
        }
        return "false";

    }

    public function put_user($id){
        $user = Input::all();
        //$v = Validator::make($user,  User::$rules);


        $user1 = User::find($id);
        $user1->update($user);
        if($user1){
            $user1 = User::with(['role'])->find($id);
            return $user1;
        }
        return "false";

    }


    public function delete_user($id){
        User::find($id)->delete();
    }


    public function doLogin()
    {
// validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required' // |alphaNum|min:6 password can only be alphanumeric and has to be greater than 3 characters
        );

// run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return "false";

        } else {

            // create our user data for the authentication
            $userdata = array(
                'email'     => Input::get('email'),
                'password'  => Input::get('password')
            );

            // attempt to do the login
            // echo Input::all();
            $user = User::where('email',Input::get('email'))->where('password', md5(Input::get('password')))->where('status','1')->with(['role'])->first();
            if ($user) {

                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)

                //return "true";
                return $user;

            } else {

                // validation not successful, send back to form
                return "false";

            }

        }
    }


    public function doRegister()
    {
// validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
        );

// run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return "false";

        } else {

            // create our user data for the authentication
            $userdata = array(
                'email'     => Input::get('email')
            );

            // attempt to do the login
            // echo Input::all();
            $user = User::where('email',Input::get('email'))->where('status','0')->with(['role'])->first();
            if ($user) {

                $password = cryptme(8);
                $user['password'] = md5($password);
                $user['status'] = "1";
                $user_array = (array) $user;
                $user->update($user_array);
                // $p = Patient::create($patient);
//               MailingPHPMailer($user['email'], $password);
                MailingPHPMailer($user['email'], $password);
                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)

                return "true";

            } else {

                // validation not successful, send back to form
                return "false";

            }

        }
    }

    public function passwordReset()
    {
// validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
        );

// run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return "false";

        } else {

            // create our user data for the authentication
            $userdata = array(
                'email'     => Input::get('email')
            );

            // attempt to do the login
            // echo Input::all();
            $user = User::where('email',Input::get('email'))->where('status','1')->with(['role'])->first();
            if ($user) {

                $password = cryptme(8);
                $user['password'] = md5($password);
                $user['status'] = "1";
                $user_array = (array) $user;
                $user->update($user_array);
                // $p = Patient::create($patient);
                ResetPasswordMaillingPHPMailer($user['email'], $password);
                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)

                return "true";

            } else {

                // validation not successful, send back to form
                return "false";

            }

        }
    }
}
