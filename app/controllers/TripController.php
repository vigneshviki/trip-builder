<?php

class TripController extends BaseController
{

    public $restful = true;
    public function get_trips()
    {
        //return  array("1"=>"hi");
        return  Trip::all();
    }

    public function get_all()
    {
        return TripAirport::with(["trip","from","to"])->get();
    }

    public function post_trip()
    {
        $trip = Input::all();


        $trip_to_insert['name'] = $trip['name'];
        $trip_create = Trip::create($trip_to_insert);
        //$trip_airport['trip_id']=$trip_create->id;


        foreach($trip['flights'] as $tl){
            $tl['trip_id'] = $trip_create->id;
            $trip_airport_create = TripAirport::create($tl);
        }

        return $trip_airport_create;


    }
    public function get_trip_byId($id)
    {
        $tripInfo = TripAirport::with(["trip","from","to"])->whereHas("trip",function($query)use($id){
            $query->where("id",$id);
        })->get();
        if($tripInfo){
            return $tripInfo;
        }else{
            return "false";
        }
    }

    public function rename_trip($id)
    {
        $tripRename = Input::all();
        $trip = Trip::find($id);
        if($trip){
            $trip->update($tripRename);
            return "true";
        }else{
            return "false";
        }
    }
}