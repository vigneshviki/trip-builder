<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TripAirport extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('trip_airport')) {
			Schema::create('trip_airport', function($table){
				$table->integer('trip_id')->unsigned();
				$table->foreign('trip_id')
						->references('id')->on('trips')
						->onDelete('cascade');
				$table->integer('from')->unsigned();
				$table->foreign('from')
						->references('id')->on('airports')
						->onDelete('cascade');
				$table->integer('to')->unsigned();
				$table->foreign('to')
						->references('id')->on('airports')
						->onDelete('cascade');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
