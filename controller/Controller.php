<?php

include_once 'RestClientConsumer.php';

if(isset($_GET["renameTrip"])){
    $trip['name'] = $_GET['name'];
    $trip['id'] = $_GET['id'];
    $trip_rename = setData("trips",$trip,$trip['id']);
    echo json_encode($trip_rename);
}

if(isset($_GET["gettripairports"])){
    $alltrips = getDatas("trips/getrips");
    echo json_encode($alltrips);
}

if(isset($_GET["deleteflight"])){
    $flight_delete = Delete("trip_airport",$_GET['id']);
    echo json_encode($flight_delete);
}

if(isset($_GET["getairports"])){
    $airports = getDatas("airports");
    echo json_encode($airports);
}

if(isset($_GET["gettripinformation"])){
    $tripInfo = getDatas2("trips",$_GET['id']);
    echo json_encode($tripInfo);
}

if(isset($_GET["addatrip"])){
    $trip['name'] = $_POST['name'];
    $trip['flights'] = array();

    foreach($_POST['trip'] as $pi){
        $flight['from'] = $pi['from'];
        $flight['to'] = $pi['to'];
        array_push($trip['flights'],$pi);
    }
    $trip_create_response = Create("trips",json_encode($trip));
    echo json_encode($trip_create_response);
}
