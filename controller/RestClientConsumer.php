<?php

include_once 'RestClient.php';
$BaseURL = "http://54.69.200.217/trip-builder/api/";
//$BaseURL = "http://127.0.0.1/trip-builder/api/";

function getDatas( $data) {
    global $BaseURL;
    $json = file_get_contents($BaseURL.$data);
    $Array = json_decode($json, true,512, JSON_BIGINT_AS_STRING);
    return $Array;
}

function getDatas2( $data, $id) {
    global $BaseURL;
    $json = file_get_contents($BaseURL.$data."/".$id);
    $Array = json_decode($json, true,512, JSON_BIGINT_AS_STRING);
    return $Array;
}

function Create($url, $data){
    //  $data_json = json_encode($data);
    global $BaseURL;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $BaseURL.$url."/create");
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=utf-8','Accept: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data)));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);
    return $response;
}

function setData($url, $data, $id){
    $data_json = json_encode($data);
    global $BaseURL;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $BaseURL.$url."/edit/".$id);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);
    return $response;
}

function setData2($url, $id){
    // $data_json = json_encode($data);
    global $BaseURL;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $BaseURL.$url."/".$id);
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);
    return $response;
}

function Delete($data, $id){
    global $BaseURL;
    $RC = new RestClient(array(
        'base_url' => $BaseURL,
        //'format' => "json",
    ));
    $result = $RC->delete($data."/delete/".$id);

    if ($result->info->http_code == 200) {
        return "true";
    }else {
        return "false";
    }
}


?>