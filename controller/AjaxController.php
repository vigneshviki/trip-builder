<?php

include_once 'service/index.php';
include_once 'RestClientConsumer.php';

if(isset($_GET["getUser"])){
    //  echo json_encode($_GET["id"]);

    $getUser = getItem("users",$_GET["id"]);
    echo json_encode($getUser);

}


if(isset($_GET["search_multiple_user"])){

    $users=getDatas("users");
    function readCSV($csvFile){
        $file_handle = fopen($csvFile, 'r');

        while (!feof($file_handle) ) {
            $line_of_text[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);
        return $line_of_text;
    }

    if(isset($_FILES['file']['tmp_name'])){
        $csvFile = $_FILES['file']['tmp_name'];
        $csv = readCSV($csvFile);

        $csvList=array();
        $usersList=array();

        for($c=0;$c<count($csv);$c++) {
            array_push($csvList,$csv[$c][0]);
        }

        for($u=0;$u<count($users);$u++) {
            array_push($usersList,$users[$u]['email']);
        }

        $usersNotActivatedList=array_diff($csvList, $usersList);
        //$_SESSION['nonreglist']=$usersNotActivatedList;
        //echo json_encode($usersNotActivatedList);

        echo json_encode($usersNotActivatedList);
    }
}
?>